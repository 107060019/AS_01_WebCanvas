var c = document.getElementById("Mycanvas");
var s = document.getElementById("size");
var ctx = c.getContext("2d");
var draw = false;
var chose;
var start_x;
var start_y;
var precanvas = new Image();
var cArray = new Array();
var ptr = -1;

function Chosesize() {
    ctx.lineWidth = document.getElementById("Brushsize").value;
    //console.log(ctx.lineWidth);
}
/*
function Chosefont() {
    ctx.lineWidth = document.getElementById("Font").value;
    //console.log(ctx.lineWidth);
}
function Chosetextsize() {
    ctx.lineWidth = document.getElementById("Textsize").value;
    //console.log(ctx.lineWidth);
}*/
/*
colorPicker.addEventListener("change", Chosecolor);

function watchColorPicker(event) {
  document.querySelectorAll("p").forEach(function(p) {
    p.style.color = event.target.value;
  });
}
*/
function Chosecolor() {
    ctx.strokeStyle = document.getElementById("colorinput").value;
    ctx.fillStyle = document.getElementById("colorinput").value;
    //console.log(document.getElementById("colorinput").value);
}

function Change(ID){
    chose = ID;
    if(ID=="Text"){
        c.style.cursor = "text";
    }else if(ID=="Undo"||ID=="Redo"||ID=="Refresh"){
        c.style.cursor = "initial";
    }else{
        c.style.cursor = "crosshair";
    }
    console.log(ID);
}

function Begin(){
   // ctx.fillRect(200,200,1,1);
    draw=true;
    //ctx.save();
    ctx.beginPath();
    start_x = event.offsetX;
    start_y = event.offsetY;
    ctx.moveTo(start_x, start_y);
    //ctx.lineWidth = size;
    Chosesize();
   // Chosecolor();
   try{
    precanvas.src = c.toDataURL("image/png")
   }catch(e){
       console.log("failed to save");
   }
   
  
   
    
}
function Draw(){
    
    switch(chose){
        case "Pen":
           
            if(draw){
                
                ctx.globalCompositeOperation = "source-over";
                ctx.lineTo(event.offsetX,event.offsetY);
                ctx.stroke();
            }
            break;
        case "Eraser":
            if(draw){
                ctx.globalCompositeOperation = 'destination-out';
                ctx.lineTo(event.offsetX,event.offsetY);
                ctx.stroke();
            }
            break;
        case "Line":
            if(draw){
                
                ctx.clearRect(0,0,c.width, c.height);
                precanvas.onload = function(){
                    ctx.drawImage(precanvas,0,0);
                }
        
                ctx.globalCompositeOperation = "source-over";
                ctx.moveTo (start_x, start_y );
                ctx.lineTo(event.offsetX, event.offsetY);
                ctx.stroke();
                
            }
            break;
    
        
           
                
    }
        
        
    
}
function End(){
    draw = false;
    ctx.closePath();
    ptr++;
    if(ptr<cArray.length){
        cArray.length=ptr;
    }
    cArray.push(document.getElementById('Mycanvas').toDataURL("image/png"));
    console.log(ptr);
    //ctx.save();
    
}
function Typing(event){
    var x = event.keyCode;
    var y =String.fromCharCode(x); 
    
    document.getElementById("Toolbox").value+=y;
        
}

function Click(){
    switch(chose){
        case "Text":
            //Chosesize();
            ctx.globalCompositeOperation = "source-over";
            ctx.font = document.getElementById("Textsize").value+"px "+document.getElementById("Font").value;
            ctx.strokeText(document.getElementById("textinput").value, event.offsetX, event.offsetY );
            //console.log( event.offsetX+','+ event.offsetY);
            break;  
        
       
    }
}
function Undo(){
    console.log(ptr);
    if(ptr > 0){
        ptr--;
        var newcanvas = new Image();
        newcanvas.src = cArray[ptr];
        
        newcanvas.onload = function(){
            ctx.clearRect(0,0,document.getElementById("Mycanvas").width, document.getElementById("Mycanvas").height);
                
            ctx.drawImage(newcanvas,0,0);
            }
        }
        console.log(ptr);
}
function Redo(){
    if(ptr<cArray.length){
        ptr++;
        var newcanvas = new Image();
        newcanvas.src = cArray[ptr];
        ctx.clearRect(0,0,c.width, c.height);
        newcanvas.onload = function(){
            ctx.clearRect(0,0,document.getElementById("Mycanvas").width, document.getElementById("Mycanvas").height);
            ctx.drawImage(newcanvas,0,0);
        }
    }
    console.log(ptr);
}
function Refresh(){
    ctx.clearRect(0,0,document.getElementById("Mycanvas").width, document.getElementById("Mycanvas").height);
}
/*
c.addEventListener("mousedown",Begin(event))
c.addEventListener("mouseup",End())
c.addEventListener("mousemove",Draw())*/