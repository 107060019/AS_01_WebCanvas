# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       |           |
| Un/Re-do button                                  | 10%       |           |
| Image tool                                       | 5%        |           |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | N         |


---

### How to use 
    工具欄中，最上方左邊的欄位是調整筆跡的粗細，右邊調整字體。
    若要輸入文字，則打在第二行左邊的欄位，右邊的欄位可選擇字體大小。
    第三行可選筆跡顏色。
    
### Function description

    

### Gitlab page link

    https://107060019.gitlab.io/AS_01_WebCanvas 

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>